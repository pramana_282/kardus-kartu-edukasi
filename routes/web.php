<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/', 'middleware' => 'tamu'], function(){
  Route::get('/','RouteController@tamu_welcome');
  Route::post('/','UserController@login');
});

Route::group(['prefix' => '/guru', 'middleware' => 'guru'], function(){
  Route::get('/','RouteController@guru');
  Route::get('/keluar','UserController@logout');
  Route::get('/{any}','RouteController@guru');
  Route::get('/{any}/{any2}','RouteController@guru');
  Route::get('/{any}/{any2}/{any3}','RouteController@guru');
  Route::get('/{any}/{any2}/{any3}/{any4}','RouteController@guru');
  Route::get('/{any}/{any2}/{any3}/{any4}/{any5}','RouteController@guru');
});

Route::group(['prefix' => '/murid', 'middleware' => 'murid'], function(){
  Route::get('/','RouteController@murid');
  Route::get('/{any}','RouteController@murid');
  Route::get('/{any}/{any2}','RouteController@murid');
  Route::get('/{any}/{any2}/{any3}','RouteController@murid');
});

// Rute API Guru/User
Route::resource('userapi','UserController');
Route::group(['prefix' => '/userapi'], function(){
  Route::post('/update_me', 'UserController@update_me');
  Route::post('/update_pass', 'UserController@update_pass');
});

// Rute API Kelas
Route::resource('/kelasapi','KelasController');
Route::group(['prefix' => '/kelasapi'], function(){
  Route::get('/guru/{username}','KelasController@show_username');
  Route::get('/kode/kode/{kode}','KelasController@show_kelas_kode');
  Route::get('/kode/publik/{kode}','KelasController@show_kelas_publik');
});

// Rute API Rute Kelas
Route::resource('rutekelasapi','RuteKelasController');

// Rute API Murid
Route::resource('muridapi','MuridController');
Route::group(['prefix' => '/muridapi'], function(){
  Route::get('/guru/{username}','MuridController@show_murid_username');
  Route::get('/kelas/{kode}','MuridController@show_murid_kode_kelas');
  Route::get('/token/{token}','MuridController@show_murid_token');

  Route::get('/delete/all/{kode}','MuridController@destroy_all');
});

// Rute API Kategori
Route::get('kategoriapi','KategoriController@index');

// Rute API HasilSoal
Route::resource('hasilsoalapi','HasilSoalController');

// Rute API Soal
Route::resource('soalapi','SoalController');
Route::group(['prefix' => '/soalapi'], function(){
  Route::get('/guru/{username}','SoalController@show_soal_username');
  Route::get('/kelas/{kode}','SoalController@show_soal_kode_kelas');
  Route::get('/kodenomor/{kode}/{nomor}','SoalController@show_soal_kode_nomor');
});

// Rute API Jawaban
Route::resource('jawabanapi','JawabanController');
Route::group(['prefix' => 'jawabanapi'], function (){
  Route::get('/kelas/{kode}/{nomor}','JawabanController@show_jawaban_kode_kelas');
  Route::get('/benar/{kode}/{nomor}','JawabanController@show_jawaban_benar');

  Route::get('/delete/all','JawabanController@destroy_all');
});

//Hasil API
Route::resource('hasilapi','HasilController');
Route::group(['prefix' => 'hasilapi'], function (){
  Route::get('/benar/{token}','HasilController@update_benar');
  Route::get('/salah/{token}','HasilController@update_salah');
  Route::get('/hasil/semua','HasilController@show_nilai');
});

//HAsil SOal API
Route::resource('hasilsoalapi','HasilSoalController');
Route::group(['prefix' => 'hasilsoalapi'], function (){
  Route::get('/show/{kelas}/{nomor}','HasilSoalController@show2');
  Route::get('/benar/{kelas}/{nomor}','HasilSoalController@update_benar');
  Route::get('/salah/{kelas}/{nomor}','HasilSoalController@update_salah');
  Route::get('/reset/all','HasilSoalController@reset');
  Route::get('/update/status/{kelas}/{nomor}','HasilSoalController@update_status');
});


// percobaan page
Route::view('/coba', 'coba');
Route::view('/tunggu', 'tunggu');
Route::view('/muridtunggu', 'muridtunggu');
Route::view('/andmainguru', 'andmainguru');
Route::view('/finalmainguru', 'finalmainguru');
Route::view('/finalmainmurid', 'finalmainmurid');
