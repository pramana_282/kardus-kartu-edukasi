function haloAnimation(){
	$('.maskot').addClass('maskotAnimated');
	$('.halo-box').addClass('halo-boxAnimated');
	$('.halo-content').addClass('halo-contentAnimation');
	$('.box-shape').addClass('box-shapeAnimation');
	$('.circle-1').addClass('circle-1Animation');
}

function homeHeight(){
	var heightDoc = $(window).height();
	$('.halo-wrepper').css({
		height : heightDoc
	});
}

// function tampilkanMasuk(){
// 	var x = $('#form-login').html();
// 	$.confirm({
// 		title: '<b style="color: dark;">Masuk dengan KARDUS!</b>',
// 		content: x,
// 		type: 'dark',
// 		typeAnimated: true,
// 		animation: 'scale',
// 		closeAnimation: 'scale',
// 		closeIcon: true,
// 		columnClass: 'm',
// 		onOpenBefore: function(){
// 		},
// 		buttons: false
// 	});
// }

// function tampilkanDaftar(){
// 	var x = $('#form-daftar').html();
// 	$.confirm({
// 		title: '<b style="color: #428bca;">Mulai bergabung dengan KARDUS!</b>',
// 		type: 'blue',
// 		typeAnimated: true,
// 		animation: 'scale',
// 		closeAnimation: 'scale',
// 		closeIcon: true,
// 		columnClass: 'm',
// 		buttons: false,
// 		content: ' ',
// 		onContentReady: function(){
// 			this.setContentAppend(x);
// 			var i = this;
// 			this.$content.find('#foto').change(function() {
// 				var img = document.getElementById('foto');
// 				var file = img.files[0];
// 				var url = URL.createObjectURL(file);
// 				console.log(url);
// 			});
// 		}
// 	});
// }

function imgValidation(){
	var fileInput = document.getElementById('foto');
	var i = fileInput.files[0];
	var c = URL.createObjectURL(i);
	$('#img-view').attr('src', c);
}

function showModal(i){
	$(i).modal('show');
}

function mainFont(that){
	var x = $(that).val();
	$('.main-q').css({
		'font-size': x+'px'
	})
}