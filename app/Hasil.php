<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Hasil extends Model
{
    protected $table = 'hasil';
    protected $primaryKey = 'id';
    protected $fillable = [
      'token',
      'kode_kelas',
      'benar',
      'salah',
    ];

    static function store_($token,$kode){
      Hasil::create([
        'token' => $token,
        'kode_kelas' => $kode,
        'benar' => '0',
        'salah' => '0',
      ]);
    }

    static function show_($token){
      return Hasil::where('token',$token)->get();
    }

    static function show_nilai_(){
      return DB::table('hasil')->join('murid','hasil.token','=','murid.token')->orderBy('benar','DESC')->get();
    }

    static function update_benar_($token){
      $data = Hasil::where('token',$token)->first();
      $j_benar = $data->benar+1;
      $data->benar = $j_benar;
      $data->save();
    }

    static function update_salah_($token){
      $data = Hasil::where('token',$token)->first();
      $j_salah = $data->salah+1;
      $data->salah = $j_salah;
      $data->save();
    }


}
