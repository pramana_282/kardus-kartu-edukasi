<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id';
    protected $fillable = [
        'username',
        'password',
        'nama',
        'jk',
        'tanggal_lahir',
        'email',
        'instansi',
        'jabatan',
        'foto',
        'alamat'
    ];

    static function login_($username,$password){
      return Auth::attempt([
        'username' => $username,
        'password' => $password,
      ],true);
    }

    static function index_(){
      if (Auth::check()) {
        return User::where('username',Auth::user()->username)->get();
      }
    }

    static function logout_(){
      return Auth::logout();
    }

    static function store_($username,$password,$nama,$jk,$tanggal_lahir,$email,$instansi,$jabatan,$foto,$alamat){
      User::create([
        'username' => $username,
        'password' => bcrypt($password),
        'nama' => $nama,
        'jk' => $jk,
        'tanggal_lahir' => $tanggal_lahir,
        'email' => $email,
        'instansi' => $instansi,
        'jabatan' => $jabatan,
        'foto' => $foto,
        'alamat' => $alamat
      ]);
    }

    static function update_me_($nama,$jk,$tanggal_lahir,$email,$instansi,$jabatan,$foto='',$alamat){
      $data = User::where('username',Auth::user()->username)->first();
      $data->nama = $nama;
      $data->jk = $jk;
      $data->tanggal_lahir = $tanggal_lahir;
      $data->email = $email;
      $data->instansi = $instansi;
      $data->jabatan = $jabatan;
      if ($foto !== '') {
        $data->foto = $foto;
      }
      $data->alamat = $alamat;
      $data->save();
    }

    static function update_pass_($password){
      $data = User::where('username',Auth::user()->username)->first();
      $data->password = bcrypt($password);
      $data->save();
    }

    static function destroy_($username){
      $data = User::where('username',$username)->first();
      $data->delete();
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
