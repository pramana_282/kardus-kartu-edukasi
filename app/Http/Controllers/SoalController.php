<?php

namespace App\Http\Controllers;

use App\Soal;
use App\HasilSoal;
use Illuminate\Http\Request;
use Auth;
use DB;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
          'soal' => 'required',
          'penjelasan' => 'required',
          // 'media' => 'mimes:jpg,jpeg,bmp,svg,gif,png,mp4,3gp,avi,webm,mkv,mp3,wav,amr',
          'kunci' => 'required|in:a1,a2,a3,a4,a5,a6',
          'kode_kelas' => 'required'
        ];

        $attr = [
          'soal' => 'Soal',
          'penjelasan' => 'Penjelasan',
          'media' => 'Media',
          'kunci' => 'Kunci jawaban',
          'kode_kelas' => 'Kode kelas'
        ];

        $msg = [
            'required' => 'wajib dimasukan',
            'mimes' => ':attribute dilarang memiliki format selain :values',
            'in' => ':attribute tidak valid'
        ];

        if ($request->media) {
          $media = $request->media;
          $media_name = $media->getClientOriginalName();
          $media_name2 = date('dmyhis').$media_name;
          $media->move('media',$media_name2);
        }
        else{
          $media_name2 = null;
        }
        $this->validate($request,$rule,$msg,$attr);
        $ceknomor = Soal::where('kode_kelas',$request->kode_kelas)->count();
        $nomor = $ceknomor+1;
        HasilSoal::store_($request->kode_kelas,$nomor);
        Soal::store_($request->soal,$request->penjelasan,$media_name2,$request->a1,$request->a2,$request->a3,$request->a4,$request->a5,$request->a6,$request->kunci,$request->kode_kelas);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Soal::show_($id);
    }

    public function show_soal_kode_kelas($kode_kelas){
      return Soal::show_soal_kode_kelas_($kode_kelas);
    }

    public function show_soal_username($username_user){
      return Soal::show_soal_username_($username_user);
    }

    public function show_soal_kode_nomor($kode,$nomor){
      return Soal::show_soal_kode_nomor_($kode,$nomor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function edit(Soal $soal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_soal)
    {
      $rule = [
        'soal' => 'required',
        'penjelasan' => 'required',
        // 'media' => 'mimes:jpg,jpeg,bmp,svg,gif,png,mp4,3gp,avi,webm,mkv,mp3,wav,amr',
        'kunci' => 'required|in:a1,a2,a3,a4,a5,a6',
      ];

      $attr = [
        'soal' => 'Soal',
        'penjelasan' => 'Penjelasan',
        'media' => 'Media',
        'kunci' => 'Kunci jawaban',
      ];

      $msg = [
          'required' => ':attribute wajib dimasukan',
          'mimes' => ':attribute dilarang memiliki format selain :values',
          'in' => ':attribute tidak valid'
      ];

      if ($request->media) {
        $media = $request->media;
        $media_name = $media->getClientOriginalName();
        $media_name2 = date('dmyhis').$media_name;
        $media->move('media',$media_name2);
      }
      else{
        $media_name2 = null;
      }
      $this->validate($request,$rule,$msg,$attr);
      Soal::update_($id_soal,$request->soal,$request->penjelasan,$media_name2,$request->a1,$request->a2,$request->a3,$request->a4,$request->a5,$request->a6,$request->kunci);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Soal  $soal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_soal)
    {
      Soal::destroy($id_soal);
    }
}
