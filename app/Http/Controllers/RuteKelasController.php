<?php

namespace App\Http\Controllers;

use App\Rute_Kelas;
use Illuminate\Http\Request;

class RuteKelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rute_Kelas  $rute_Kelas
     * @return \Illuminate\Http\Response
     */
    public function show($kode_kelas)
    {
        return Rute_Kelas::show_($kode_kelas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rute_Kelas  $rute_Kelas
     * @return \Illuminate\Http\Response
     */
    public function edit(Rute_Kelas $rute_Kelas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rute_Kelas  $rute_Kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_kelas)
    {
      Rute_Kelas::update_($kode_kelas,$request->rute);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rute_Kelas  $rute_Kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rute_Kelas $rute_Kelas)
    {
        //
    }
}
