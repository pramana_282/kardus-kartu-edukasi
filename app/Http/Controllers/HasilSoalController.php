<?php

namespace App\Http\Controllers;

use App\HasilSoal;
use Illuminate\Http\Request;

class HasilSoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        HasilSoal::store_($request->kelas,$request->nomor);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HasilSoal  $hasilSoal
     * @return \Illuminate\Http\Response
     */
     public function show()
     {
         //
     }

     public function show2($kelas,$soal)
     {
         return HasilSoal::show2_($kelas,$soal);
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HasilSoal  $hasilSoal
     * @return \Illuminate\Http\Response
     */
    public function edit(HasilSoal $hasilSoal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HasilSoal  $hasilSoal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$kelas,$nomor)
    {
        // HasilSoal::update_($kelas,$nomor);
    }

    public function update_status($kelas,$nomor){
      HasilSoal::update_($kelas,$nomor);
    }

    public function update_benar($kelas,$nomor){
      HasilSoal::update_benar_($kelas,$nomor);
    }

    public function update_salah($kelas,$nomor){
      HasilSoal::update_salah_($kelas,$nomor);
    }

    public function reset(){
      HasilSoal::reset_();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HasilSoal  $hasilSoal
     * @return \Illuminate\Http\Response
     */
    public function destroy($kelas)
    {
        HasilSoal::destroy_($kelas);
    }
}
