<?php

namespace App\Http\Controllers;

use App\Murid;
use Illuminate\Http\Request;
use App\Hasil;

class MuridController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
          'nama' => 'required',
          'foto' => 'required|image|mimes:jpg,bmp,jpeg,png,svg'
        ];

        $attr = [
          'nama' => 'Namamu',
          'foto' => 'Fotomu'
        ];

        $msg = [
          'required' => ':attribute wajib dimasukan',
          'image' => ':attribute harus berupa gambar',
          'mimes' => ':attribute harus memiliki format :values',
        ];
        $this->validate($request,$rule,$msg,$attr);
        $foto = $request->foto;
        $foto_name = $foto->getClientOriginalName();
        $foto_name2 = date('hisdmy').$foto_name;
        $foto->move('img/murid/',$foto_name2);
        $token = md5($request->nama.$foto_name2);
        Hasil::store_($token,$request->kode_kelas);
        Murid::store_($request->nama,$foto_name2,$request->kode_kelas,$token);
        return response()->json([
          'token' => $token
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Murid  $murid
     * @return \Illuminate\Http\Response
     */
    public function show(Murid $murid)
    {
        //
    }

    public function show_murid_username($username_user){
      return Murid::show_murid_username_($username_user);
    }

    public function show_murid_kode_kelas($kode_kelas){
      return Murid::show_murid_kode_kelas_($kode_kelas);
    }

    public function show_murid_token($token){
      return Murid::show_murid_token_($token);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Murid  $murid
     * @return \Illuminate\Http\Response
     */
    public function edit(Murid $murid)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Murid  $murid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Murid $murid)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Murid  $murid
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Murid::destroy_($id);
    }

    public function destroy_all($kode){
      Murid::destroy_all_($kode);
    }
}
