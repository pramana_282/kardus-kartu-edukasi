<?php

namespace App\Http\Controllers;

use App\Kelas;
use App\Rute_kelas;
use Illuminate\Http\Request;
use Auth;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
          $rule = [
            'kode' => 'required|unique:kelas,kode',
            'nama' => 'required',
            'kategori' => 'required|numeric'
          ];
          $attr = [
            'kode' => 'Kode kelas',
            'nama' => 'Nama kelas',
            'kategori' => 'Kategori'
          ];
          $msg = [
            'unique' => ':attribute sudah pernah dipakai',
            'required' => ':attribute wajib dimasukan',
            'numeric' => ':attribute tidak valid'
          ];
          $this->validate($request,$rule,$msg,$attr);
          Kelas::store_(Auth::user()->username,$request->kode,$request->nama,$request->kategori,'0');
          Rute_Kelas::store_($request->kode,'0');
        }
        else{
          echo "Hmm.. gak ada sesi login gak boleh utak atik api";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
     public function show_username($username_user)
     {
       return Kelas::show_username_($username_user);
     }

     public function show_kelas_kode($kode)
     {
       return Kelas::show_kelas_kode_($kode);
     }

    public function show_kelas_publik($kode){
      if (!empty($kode)) {
        return Kelas::show_kelas_publik_($kode);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelas $kelas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
      $rule = [
        'nama' => 'required',
        'kategori' => 'required|numeric',
        'status' => 'required|in:0,1'
      ];
      $attr = [
        'nama' => 'Nama kelas',
        'kategori' => 'Kategori',
        'status' => 'Status kelas'
      ];
      $msg = [
        'required' => ':attribute wajib dimasukan',
        'numeric' => ':attribute tidak valid',
        'in' => ':attribute tidak valid'
      ];
      $this->validate($request,$rule,$msg,$attr);
      Kelas::update_($kode,$request->nama,$request->kategori,$request->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        Kelas::destroy_($kode);
    }
}
