<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function tamu_welcome(){
      return view('tamu.halo');
    }

    public function guru(){
      return view('guru.guru');
    }

    public function murid(){
      return view('murid.murid');
    }
}
