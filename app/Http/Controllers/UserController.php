<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return User::index_(); // Data User Yang Sedang Login
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
      $username = $request->username;
      $password = $request->password;
      $cek = User::login_($username,$password);
      if ($cek) {
        return redirect('/guru');
      }
      else{
        return back()->with('errorlogin','Papan nama atau kunci yang dimasukan salah!');
      }
    }

    public function logout(){
      User::logout_();
      return redirect('/');
    }

    public function store(Request $request)
    {
        $rule = [
          'username' => 'required|unique:users,username|alpha_dash',
          'password' => 'required|min:6',
          'nama' => 'required|max:33',
          'jk' => 'required|in:1,2',
          'tanggal_lahir' => 'required|date',
          'email' => 'required|email',
          'instansi' => 'required',
          'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ];
        $attr = [
          'username' => 'Papan nama anda',
          'password' => 'Kunci anda',
          'nama' => 'Nama lengkap anda',
          'jk' => 'Jenis kelamin anda',
          'tanggal_lahir' => 'Tanggal lahir anda',
          'email' => 'Email anda',
          'instansi' => 'Instansi/Asal Sekolah anda',
          'foto' => 'Foto anda',
        ];
        $msg = [
          'alpha_dash' => ':attribute hanya diizinkan untuk memakai karakter huruf, nomor, garis bawah (_) dan tanda hubung (-)',
          'required' => ':attribute wajib dimasukan',
          'unique' => ':attribute sudah pernah digunakan. coba yang lain',
          'min' => ':attribute harus minimal :min karakter',
          'max' => ':attribute harus maksimal :max karakter',
          'in' => ':attribute tidak valid',
          'date' => ':attribute harus memiliki format tanggal dengan benar',
          'email' => ':attribute harus memiliki format email dengan benar',
          'image' => ':attribute harus berupa gambar',
          'mimes' => ':attribute harus memiliki format :values'
        ];
        $this->validate($request,$rule,$msg,$attr);
        $username = $request->username;
        $password = $request->password;
        $nama = $request->nama;
        $jk = $request->jk;
        $tanggal_lahir = $request->tanggal_lahir;
        $email = $request->email;
        $instansi = $request->instansi;
        $jabatan = $request->jabatan;
        $alamat = $request->alamat;
        $foto = $request->foto;
        $foto_name = $foto->getClientOriginalName();
        $foto_name2 = date('dmyhis').$foto_name;
        $foto->move('img/guru/',$foto_name2);
        User::store_($username,$password,$nama,$jk,$tanggal_lahir,$email,$instansi,$jabatan,$foto_name2,$alamat);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $username
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $username
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $username
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        //
    }

    public function update_me(Request $request){
      $rule = [
        'nama' => 'required|max:33',
        'jk' => 'required|in:1,2',
        'tanggal_lahir' => 'required|date',
        'email' => 'required|email',
        'instansi' => 'required',
        // 'foto' => 'image|mimes:jpeg,png,jpg,gif,svg',
      ];
      $attr = [
        'nama' => 'Nama lengkap anda',
        'jk' => 'Jenis kelamin anda',
        'tanggal_lahir' => 'Tanggal lahir anda',
        'email' => 'Email anda',
        'instansi' => 'Instansi/Asal Sekolah anda',
        'foto' => 'Foto anda',
      ];
      $msg = [
        'required' => ':attribute wajib dimasukan',
        'unique' => ':attribute sudah pernah digunakan. coba yang lain',
        'max' => ':attribute harus maksimal :max karakter',
        'in' => ':attribute tidak valid',
        'date' => ':attribute harus memiliki format tanggal dengan benar',
        'email' => ':attribute harus memiliki format email dengan benar',
        'image' => ':attribute harus berupa gambar',
        'mimes' => ':attribute harus memiliki format :values'
      ];
      $this->validate($request,$rule,$msg,$attr);
      $username = $request->username;
      $nama = $request->nama;
      $jk = $request->jk;
      $tanggal_lahir = $request->tanggal_lahir;
      $email = $request->email;
      $instansi = $request->instansi;
      $jabatan = $request->jabatan;
      $alamat = $request->alamat;
      if ($request->foto) {
        $foto = $request->foto;
        $foto_name = $foto->getClientOriginalName();
        $foto_name2 = date('dmyhis').$foto_name;
        $foto->move('img/guru/',$foto_name2);
      }
      else{
        $foto_name2 = '';
      }
      $this->validate($request,$rule,$msg,$attr);
      User::update_me_($nama,$jk,$tanggal_lahir,$email,$instansi,$jabatan,$foto_name2,$alamat);
    }

    public function update_pass(Request $request){
      $rule = [
        'passwordbaru' => 'required|min:6',
        'passwordbaru2' => 'required|same:passwordbaru'
      ];

      $attr = [
        'passwordbaru' => 'Kunci baru',
        'passwordbaru2' => 'Konfirmasi kunci baru'
      ];

      $msg = [
        'required' => ':attribute wajib dimasukan',
        'same' => ':attribute harus sama dengan :other',
        'min' => ':attribute harus berisi minimal :min karakter'
      ];
      $this->validate($request,$rule,$msg,$attr);
      User::update_pass_($request->passwordbaru);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $username
     * @return \Illuminate\Http\Response
     */
    public function destroy($username)
    {
        //
    }
}
