<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Murid extends Model
{
    protected $table = 'murid';
    protected $primaryKey = 'id';
    protected $fillable = [
      'nama',
      'foto',
      'kode_kelas',
      'token'
    ];

    static function store_($nama,$foto,$kode_kelas,$token){
      Murid::create([
        'nama' => $nama,
        'foto' => $foto,
        'kode_kelas' => $kode_kelas,
        'token' => $token
      ]);
    }

    static function show_murid_username_($username_user){
      return DB::table('murid as m')->leftjoin('kelas as k','m.kode_kelas','=','k.kode')->select('m.id','m.nama','m.foto','m.kode_kelas')->orderBy('m.id','desc')->where('k.username_user',$username_user)->get();
    }

    static function show_murid_kode_kelas_($kode_kelas){
      return Murid::select('id','nama','foto','kode_kelas')->orderBy('id','desc')->where('kode_kelas',$kode_kelas)->get();
    }

    static function show_murid_token_($token){
      return Murid::select('id','nama','foto','kode_kelas')->where('token',$token)->get();
    }

    static function destroy_($id){
      $data = Murid::where('id',$id)->first();
      $data->delete();
    }

    static function destroy_all_($kode_kelas){
      $data = Murid::where('kode_kelas',$kode_kelas)->truncate();
      $data->delete();
    }
}
