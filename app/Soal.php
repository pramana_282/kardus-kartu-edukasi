<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\HasilSoal;
use Auth;

class Soal extends Model
{
    protected $table = 'soal';
    protected $primaryKey = 'id';
    protected $fillable = [
      'nomor',
      'soal',
      'penjelasan',
      'media',
      'a1',
      'a2',
      'a3',
      'a4',
      'a5',
      'a6',
      'kunci',
      'kode_kelas'
    ];

    static function store_($soal,$penjelasan,$media,$a1,$a2,$a3,$a4,$a5,$a6,$kunci,$kode_kelas){
      $ceknomor = Soal::where('kode_kelas',$kode_kelas)->count();
      $nomor = $ceknomor+1;
      Soal::create([
        'nomor' => $nomor,
        'soal' => $soal,
        'penjelasan' => $penjelasan,
        'media' => $media,
        'a1' => $a1,
        'a2' => $a2,
        'a3' => $a3,
        'a4' => $a4,
        'a5' => $a5,
        'a6' => $a6,
        'kunci' => $kunci,
        'kode_kelas' => $kode_kelas
      ]);
    }

    static function show_soal_username_($username_user){
      return DB::table('soal as s')->join('kelas as k','s.kode_kelas','=','k.kode')->select('s.id','s.nomor','s.soal','s.penjelasan','s.media','s.a1','s.a2','s.a3','s.a4','s.a5','s.a6','s.kode_kelas')->where('k.username_user',$username_user)->get();
    }

    static function show_soal_kode_kelas_($kode_kelas){
      return Soal::where('kode_kelas',$kode_kelas)->get();
    }

    static function show_soal_kode_nomor_($kode_kelas,$nomor){
      return Soal::where('kode_kelas',$kode_kelas)->where('nomor',$nomor)->get();
    }

    static function show_($id){
      if (Auth::check()) {
        return DB::table('soal as s')->join('kelas as k','s.kode_kelas','=','k.kode')->select('s.id','s.nomor','s.soal','s.kunci','s.penjelasan','s.media','s.a1','s.a2','s.a3','s.a4','s.a5','s.a6','s.kode_kelas')->where('s.id',$id)->where('k.username_user',Auth::user()->username)->get();
      }
    }

    static function update_($id,$soal,$penjelasan,$media='',$a1,$a2,$a3,$a4,$a5,$a6,$kunci){
      $data = Soal::where('id',$id)->first();
      $data->soal = $soal;
      $data->penjelasan = $penjelasan;
      if ($data->media) {
        $data->media = $media;
      }
      $data->a1 = $a1;
      $data->a2 = $a2;
      $data->a3 = $a3;
      $data->a4 = $a4;
      $data->a5 = $a5;
      $data->a6 = $a6;
      $data->kunci = $kunci;
      $data->save();
    }

    static function destroy_($id){
      $data = DB::table('soal as s')->join('kelas as k','s.kode_kelas','=','k.kode')->select('s.id','s.kode_kelas')->where('s.id',$id)->where('k.username_user',Auth::user()->username)->first();
      $data->delete();
    }
}
