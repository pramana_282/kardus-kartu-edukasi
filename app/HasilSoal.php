<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class HasilSoal extends Model
{
    protected $table = 'hasil_soal';
    protected $primaryKey = 'id';
    protected $fillable = [
      'kelas',
      'nomor',
      'benar',
      'salah',
      'status'
    ];

    static function store_($kelas,$nomor){
      HasilSoal::create([
        'kelas' => $kelas,
        'nomor' => $nomor,
        'benar' => '0',
        'salah' => '0',
        'status' => '0'
      ]);
    }

    static function update_benar_($kelas,$nomor){
      $data = HasilSoal::where('kelas',$kelas)->where('nomor',$nomor)->first();
      $data->benar = $data->benar+1;
      $data->save();
    }

    static function reset_(){
      DB::table('hasil_soal')->update(['benar' => 0, 'salah' => 0, 'status' => '0']);
    }

    static function update_($kelas,$nomor){
      $data = HasilSoal::where('kelas',$kelas)->where('nomor',$nomor)->first();
      $data->status = '1';
      $data->save();
    }

    static function update_salah_($kelas,$nomor){
      $data = HasilSoal::where('kelas',$kelas)->where('nomor',$nomor)->first();
      $data->salah = $data->salah+1;
      $data->save();
    }

    static function show2_($kelas,$soal){
      return HasilSoal::where('kelas',$kelas)->where('nomor',$soal)->get();
    }

    //
    // static function destroy_($id){
    //   $data = HasilSoal::where('nomor',$nomor)->where('kelas',$kelas);
    //   $data->delete();
    // }
}
