<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Jawaban extends Model
{
    protected $table = 'jawaban';
    protected $primaryKey = 'id';
    protected $fillable = [
      'id_murid',
      'id_soal',
      'jawaban',
      'waktu'
    ];

    static function store_($id_murid,$id_soal,$jawaban,$waktu){
      Jawaban::create([
        'id_murid' => $id_murid,
        'id_soal' => $id_soal,
        'jawaban' => $jawaban,
        'waktu' => $waktu
      ]);
    }

    static function show_jawaban_kode_kelas_($kode_kelas,$nomor){
      return DB::table('jawaban as j')->join('soal as s','j.id_soal','=','s.id')->select('id_murid','id_soal','jawaban','waktu')->where('s.kode_kelas',$kode_kelas)->where('s.nomor',$nomor)->get();
    }

    static function show_jawaban_benar_($kode_kelas,$nomor){
      return DB::table('jawaban as j')->join('soal as s',function($join){
        $join->on('j.id_soal','=','s.id')->on('j.jawaban','=','s.kunci');
      })->select('id_murid','id_soal','jawaban','waktu')->where('s.kode_kelas',$kode_kelas)->where('s.nomor',$nomor)->get();
    }

    static function destroy_all_(){
      $data = Jawaban::truncate();
      $data->delete();
    }
}
