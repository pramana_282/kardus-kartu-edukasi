<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rute_Kelas extends Model
{
    protected $table = 'rute_kelas';
    protected $primaryKey = 'id';
    protected $fillable = [
      'kode_kelas',
      'rute'
    ];

    static function show_($kode_kelas){
      return Rute_Kelas::where('kode_kelas',$kode_kelas)->get();
    }

    static function store_($kode_kelas,$rute){
      Rute_Kelas::create([
        'kode_kelas' => $kode_kelas,
        'rute' => $rute
      ]);
    }

    static function update_($kode_kelas,$rute){
      $data = Rute_Kelas::where('kode_kelas',$kode_kelas)->first();
      $data->rute = $rute;
      $data->save();
    }

    static function destroy_($kode_kelas){
      $data = Rute_Kelas::where('kode_kelas',$kode_kelas)->first();
      $data->delete();
    }
}
