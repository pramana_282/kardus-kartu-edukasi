<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $primaryKey = 'id';
    protected $fillable = [
      'username_user',
      'kode',
      'nama',
      'id_kategori',
      'status'
    ];

    static function show_username_($username_user){
      return DB::table('kelas as kls')->join('kategori as ktg','kls.id_kategori','=','ktg.id')->select('username_user','kode','nama','nama_kategori','status')->where('username_user',$username_user)->get();
    }

    static function show_kelas_kode_($kode){
      return DB::table('kelas as k')->join('users as u','k.username_user','=','u.username')->select('u.jk','u.nama as nama_user','k.kode','k.id_kategori','k.status','k.nama')->where('kode',$kode)->get();
    }

    static function show_kelas_publik_($kode){
      return Kelas::where('kode',$kode)->where('status','1')->get();
    }

    static function store_($username_user,$kode,$nama,$id_kategori,$status){
      Kelas::create([
        'username_user' => $username_user,
        'kode' => strtolower($kode),
        'nama' => $nama,
        'id_kategori' => $id_kategori,
        'status' => $status
      ]);
    }

    static function update_($kode,$nama,$id_kategori,$status){
      $data = Kelas::where('kode',$kode)->first();
      $data->nama = $nama;
      $data->id_kategori = $id_kategori;
      $data->status = $status;
      $data->save();
    }

    static function destroy_($kode){
      $data = Kelas::where('kode',$kode)->first();
      $data->delete();
    }
}
