<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $primaryKey = 'id';
    protected $fillable = [
      'nama'
    ];

    static function index_(){
      return Kategori::orderBy('nama_kategori',"ASC")->get();
    }
}
