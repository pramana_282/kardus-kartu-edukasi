require('./bootstrap');
window.Vue = require('vue');
import VueRouter from 'vue-router';
import VueSession from 'vue-session';

Vue.use(VueRouter);
Vue.use(VueSession);

const GuruRouter = new VueRouter({
  mode:'history',
  routes:[
    {
      path:'/guru',
      name:'Guru',
      component: require('./components/guru/Dasbor.vue').default
    },
    {
      path:'/guru/profil',
      name:'Profil',
      component: require('./components/guru/Profil.vue').default
    },
    {
      path:'/guru/kelas/:kode/pengaturan',
      name:'PengaturanKelas',
      component: require('./components/guru/PengaturanKelas.vue').default
    },
    {
      path:'/guru/kelas/:kode/soal',
      name:'Soal',
      component: require('./components/guru/Soal.vue').default
    },
    {
      path:'/guru/kelas/:kode/soal/tambah',
      name:'TambahSoal',
      component: require('./components/guru/TambahSoal.vue').default
    },
    {
      path:'/guru/kelas/:kode/soal/id/:id',
      name:'EditSoal',
      component: require('./components/guru/EditSoal.vue').default
    },
    {
      path:'/guru/kelas/:kode/main/:nomor',
      name:'Main',
      component: require('./components/guru/Main.vue').default
    },
    {
      path:'/guru/kelas/:kode/main/:nomor/penjelasan',
      name:'Penjelasan',
      component: require('./components/guru/Penjelasan.vue').default
    },
    {
      path:'/guru/kelas/:kode/siapmain',
      name:'SiapMain',
      component: require('./components/guru/SiapMain.vue').default
    },
    {
      path:'/guru/kelas/:kode/hasil',
      name:'HAsil',
      component: require('./components/guru/Hasil.vue').default
    },
  ]
});

const MuridRouter = new VueRouter({
  mode:'history',
  routes:[
    {
      path:'/murid',
      name:'Murid',
      component: require('./components/murid/Halo.vue').default
    },
    {
      path:'/murid/:kode/main/:nomor',
      name:'Main',
      component: require('./components/murid/Main.vue').default
    },
    {
      path:'/murid/:kode/siapmain',
      name:'SiapMain',
      component: require('./components/murid/SiapMain.vue').default
    },
    {
      path:'/murid/:kode/hasil',
      name:'Hasil',
      component: require('./components/murid/Hasil.vue').default
    },
  ]
});

Vue.component('tamu-halo', require('./components/tamu/Halo.vue').default);

window.onload = function(){
  const tamu = new Vue({
    el:'#tamu'
  });

  const guru = new Vue({
    el:'#guru',
    router:GuruRouter,
  });

  const murid = new Vue({
    el:'#murid',
    router:MuridRouter
  });

  //OTOMATISASI CSRF PADA FORM METHOD POST. JANGAN DIGANGGU
  var csrf_token = $("meta[name='csrf-token']").attr("content");
  $("form").prepend('<input type="hidden" name="_token" value="'+csrf_token+'" />');
}
