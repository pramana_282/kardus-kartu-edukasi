<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Percobaan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('plugin/jquery-confirm/jquery-confirm.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/sementara.css')}}">
	<link rel="stylesheet" href="{{asset('plugin/circle.css')}}">
	<script src="{{asset('js/functions.js')}}"></script>
	<script src="{{asset('js/app.js')}}" charset="utf-8"></script>
	<script src="{{asset('plugin/jquery-confirm/jquery-confirm.min.js')}}"></script>
</head>
<body style="padding: 10;">


	<div class="final-guru">
		<div class="text-center m-3">
			<h1>Hasil</h1>
		</div>

		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="no">No.</th>
					<th>Nama</th>
					<th class="fit">Benar</th>
					<th class="fit">Salah</th>
					<th class="fit">Poin</th>
					<th class="fit">Rangking</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="c">1.</td>
					<td>Fanesa  Hadi Pramana</td>
					<td class="c">2</td>
					<td class="c">3</td>
					<td class="c">9</td>
					<td class="c">
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100">25%</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="c">2.</td>
					<td>tegar  santosa</td>
					<td class="c">3</td>
					<td class="c">8</td>
					<td class="c">100</td>
					<td class="c">
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>

	</div>

	<script>
		function statistikData(){
			var ini = $('.ini').html();
			$.confirm({
				title: false,
				content: ini,
				type: 'blue',
				typeAnimated: true,
				animation: 'scale',
				closeAnimation: 'scale',
				closeIcon: true,
				columnClass: 'l',
				buttons: false,
				onOpenBefore: function(){

				},
			});
		}

		function lihatPenjelasan(){
			$.confirm({
				title: false,
				content: 'Ini adalah penjelasannya!!! Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	',
				type: 'blue',
				typeAnimated: true,
				animation: 'scale',
				closeAnimation: 'scale',
				closeIcon: true,
				columnClass: 'sm',
				buttons: false,
				onOpenBefore: function(){

				},
			});
		}
		$(document).ready(function(){

			$('.box-s').each(function(i, e) {
				var color = $(e).data('color');
				var val = $(e).data('val');
				$(e).find('span').text(val+'%');


				$(e).css({
					background: color,
					height: val+'px'
				});
			});


		});
	</script>

</body>
</html>