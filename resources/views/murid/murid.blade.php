@extends('layout')
@section('title','Mulai Bermain!')

@section('head')
  <link rel="stylesheet" href="{{asset('plugin/jquery-confirm/jquery-confirm.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="{{asset('css/m-style.css')}}">
  <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" href="{{asset('plugin/loader/css-loader.css')}}">
  <link rel="stylesheet" href="{{asset('plugin/circle.css')}}">
  <script src="{{asset('js/functions.js')}}"></script>
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>
  <script src="{{asset('plugin/jquery-confirm/jquery-confirm.min.js')}}"></script>

  {{-- CUSTOM DISINI --}}
@endsection

@section('isi-body')
  <div id="murid">
    <router-view></router-view>
  </div>
@endsection
