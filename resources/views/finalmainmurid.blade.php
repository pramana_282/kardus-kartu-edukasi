<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Percobaan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('plugin/jquery-confirm/jquery-confirm.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/m-style.css')}}">
	<link rel="stylesheet" href="{{asset('css/sementara.css')}}">
	<link rel="stylesheet" href="{{asset('plugin/circle.css')}}">
	<script src="{{asset('js/functions.js')}}"></script>
	<script src="{{asset('js/app.js')}}" charset="utf-8"></script>
	<script src="{{asset('plugin/jquery-confirm/jquery-confirm.min.js')}}"></script>
</head>
<body style="padding: 10;">


	<div class="final-murid">
		<div class="text-center mb-4 border-bottom border-info">
			<h3>Hasil Akhir</h3>
		</div>
		<div class="row">
			<div class="col-sm-6 mt-3">
				<div class="c100 p22 center big">
					<span>22</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
				<div class="text-center mt-3">
					<p>Jawaban Benar</p>
				</div>
			</div>

			<div class="col-sm-6 mt-3">
				<div class="c100 p22 center big danger">
					<span>22</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
				<div class="text-center mt-3">
					<p>Jawaban Salah</p>
				</div>
			</div>
		</div>

		<div class="final-skor mt-4">
			<div class="p-3 mb-2 rounded bg-dark text-white text-center">
				<b>Total : </b>
				<span>68%</span>
			</div>
		</div>

		<div class="text-center mt-3">
			<button class="btn btn-danger btn-xs">Keluar</button>
		</div>

		<div class="text-black text-center copy">
			© 2019 Fadhigar Project - KARDUS Kartu Edukasi
		</div>
	</div>

	<script>
		$(document).ready(function(){


		});
	</script>

</body>
</html>