{{--  KARDUS - FANESA, DHIMAS, TEGAR--}}
<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>@yield('title') | KARDUS - Kartu Edukasi</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
  <link rel="icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  @yield('head')
</head>
<body>

  <div id="loader"><img src="{{asset('img/loader.gif')}}" alt="Memuat.."><br><b>KARDUS</b> - KARTU EDUKASI<br><br><span id="msg">Memuat...</span></div>
  @yield('isi-body')

  <script type="text/javascript">
    $(document).ready(function(){
      title = ' | KARDUS - Kartu Edukasi';
    });
  </script>
</body>
</html>
