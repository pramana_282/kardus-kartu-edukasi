@extends('layout')
@section('title','Halo Guru!')

@section('head')
  <link rel="stylesheet" href="{{asset('plugin/jquery-confirm/jquery-confirm.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
  <link rel="stylesheet" href="{{asset('plugin/circle.css')}}">
  <script src="{{asset('js/functions.js')}}"></script>
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>
  <script src="{{asset('plugin/jquery-confirm/jquery-confirm.min.js')}}"></script>

  {{-- CUSTOM DISINI --}}

@endsection

@section('isi-body')

  <div id="guru">
    <nav class="navbar navbar-expand-lg navbar-dark pl-5 pr-5 bg-dark">
      <router-link :to="{name:'Guru'}" class="navbar-brand das-logo" tag="a"><img src="{{asset('img/text-logo.png')}}" alt="KARDUS"></router-link>

      <div class="main-nav">
        <ul>
          <li class="main-n">
            <router-link :to="{name:'Guru'}" tag="a" class="nav-link">Beranda</router-link>
          </li>
          <li class="main-n">
            <router-link :to="{name:'Profil'}" tag="a" class="nav-link">Profil</router-link>
          </li>
          <li>
            <a class="link btn btn-danger btn-xs" href="{{url('/guru/keluar')}}">Keluar</a>
          </li>
        </ul>
      </div>

      <!-- <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <router-link :to="{name:'Guru'}" tag="a" class="nav-link">Beranda</router-link>
          </li>
          <li class="nav-item">
            <router-link :to="{name:'Profil'}" tag="a" class="nav-link">Profil</router-link>
          </li>
          <li class="nav-item">
              <a class="link btn btn-danger btn-sm" href="{{url('/guru/keluar')}}">Keluar</a>
          </li>
        </ul>
      </div> -->
    </nav>

    <router-view :guru="{{json_encode(Auth::user()->username)}}"></router-view>
  </div>

  <div class="bg-dark text-white text-center p-3 footer">
    &copy 2019 Arsan Dev - KARDUS Kartu Edukasi
  </div>
  </script>

  <!-- <script src="('plugin/summernote/summernote.min.js')"></script> -->
@endsection
