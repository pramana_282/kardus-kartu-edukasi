@extends('layout')
@section('title','Selamat Datang')

@section('head')
  <link rel="stylesheet" href="{{asset('plugin/jquery-confirm/jquery-confirm.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="stylesheet" href="{{asset('plugin/summernote/summernote-lite.css')}}">
  <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
  <script src="{{asset('js/functions.js')}}"></script>
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>
  <script src="{{asset('plugin/jquery-confirm/jquery-confirm.min.js')}}"></script>

  {{-- CUSTOM DISINI --}}

@endsection

@section('isi-body')
  <div id="tamu">
    <?php $errorlogin = Session::get('errorlogin');?>
    <tamu-halo :errorlogin="{{json_encode($errorlogin)}}"></tamu-halo>
  </div>
@endsection
