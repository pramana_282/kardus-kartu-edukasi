<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Percobaan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<meta name="csrf-token" content="{{csrf_token()}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('plugin/jquery-confirm/jquery-confirm.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/sementara.css')}}">
	<link rel="stylesheet" href="{{asset('plugin/circle.css')}}">
	<script src="{{asset('js/functions.js')}}"></script>
	<script src="{{asset('js/app.js')}}" charset="utf-8"></script>
	<script src="{{asset('plugin/jquery-confirm/jquery-confirm.min.js')}}"></script>
</head>
<body style="padding: 10;">





	<div class="and-q">
		<div class="and-q-wrepperq">

			<div class="statistik border-bottom mt-4 mb-5">
				<div class="box-s" data-color="pink" data-val="70" onclick="statistikData()">
					<span>0%</span>
				</div>

				<div class="box-s" data-color="orange" data-val="20" onclick="statistikData()">	
					<span>0%</span>
				</div>

				<div class="box-s" data-color="gray" data-val="100" onclick="statistikData()">	
					<span>0%</span>
				</div>

				<div class="box-s" data-color="blue" data-val="20" onclick="statistikData()">	
					<span>0%</span>
				</div>

				<div class="box-s" data-color="silver" data-val="45" onclick="statistikData()">	
					<span>0%</span>
				</div>

				<div class="box-s" data-color="#388a" data-val="90" onclick="statistikData()">	
					<span>0%</span>
				</div>

			</div>

			<div class="circle progress-view">
				<div class="row width100">
					<div class="col-6">
						<div class="c100 p22 center big">
							<span>22%</span>
							<div class="slice">
								<div class="bar"></div>
								<div class="fill"></div>
							</div>
						</div>
						<h4 class="text-center mt-2">Benar</h4>
					</div>
					<div class="col-6">
						<div class="c100 p12 danger big center">
							<span>12%</span>
							<div class="slice">
								<div class="bar"></div>
								<div class="fill"></div>
							</div>
						</div>
						<h4 class="text-center mt-2">Salah</h4>
					</div>
				</div>
			</div>

			<div class="button text-center mt-5">
				<button class="btn btn-primary" onclick="lihatPenjelasan();">Lihat Penjelasan</button>
				<button class="btn btn-success">Lanjut</button>
			</div>
		</div>
	</div>

	
	<template class="ini">
		<table class="table mt-3">
			<tr>
				<td>
					<span class="new-murid">
						<img src="img/murid/054949270319download.png" alt="Foto Murid01">
					</span>
					<span>
						asd
					</span>
				</td>
			</tr>
			<tr>
				<td>
					<span class="new-murid">
						<img src="img/murid/054949270319download.png" alt="Foto Murid01">
					</span>
					<span>
						asd
					</span>
				</td>
			</tr>
			<tr>
				<td>
					<span class="new-murid">
						<img src="img/murid/054949270319download.png" alt="Foto Murid01">
					</span>
					<span>
						asd
					</span>
				</td>
			</tr>
		</table>
	</template>

	<script>
		function statistikData(){
			var ini = $('.ini').html();
			$.confirm({
				title: false,
				content: ini,
				type: 'blue',
				typeAnimated: true,
				animation: 'scale',
				closeAnimation: 'scale',
				closeIcon: true,
				columnClass: 'l',
				buttons: false,
				onOpenBefore: function(){

				},
			});
		}

		function lihatPenjelasan(){
			$.confirm({
				title: false,
				content: 'Ini adalah penjelasannya!!! Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	',
				type: 'blue',
				typeAnimated: true,
				animation: 'scale',
				closeAnimation: 'scale',
				closeIcon: true,
				columnClass: 'sm',
				buttons: false,
				onOpenBefore: function(){

				},
			});
		}
		$(document).ready(function(){

			$('.box-s').each(function(i, e) {
				var color = $(e).data('color');
				var val = $(e).data('val');
				$(e).find('span').text(val+'%');


				$(e).css({
					background: color,
					height: val+'px'
				});
			});


		});
	</script>

</body>
</html>