<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasilSoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_soal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kelas',191);
            $table->bigInteger('nomor_soal')->unsigned();
            $table->bigInteger('benar')->unsigned();
            $table->bigInteger('salah')->unsigned();
            $table->foreign('nomor_soal')->references('nomor')->on('soal')->onDelete('cascade');
            $table->foreign('kelas')->references('kode')->on('soal')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_soal');
    }
}
