<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username_user',191);
            $table->string('kode',191)->unique();
            $table->string('nama',191);
            $table->bigInteger('id_kategori')->unsigned();
            $table->enum('status',['1','0']);
            $table->foreign('username_user')->references('username')->on('users')->ondelete('cascade');
            $table->foreign('id_kategori')->references('id')->on('kategori')->ondelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas');
    }
}
