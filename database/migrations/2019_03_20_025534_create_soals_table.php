<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nomor')->unsigned();
            $table->text('soal');
            $table->text('penjelasan');
            $table->text('media');
            $table->string('a1',191);
            $table->string('a2',191);
            $table->string('a3',191);
            $table->string('a4',191);
            $table->string('a5',191);
            $table->string('a6',191);
            $table->enum('kunci',['a1','a2','a3','a4','a5','a6']);
            $table->string('kode_kelas',191);
            $table->foreign('kode_kelas')->references('kode')->on('kelas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soal');
    }
}
