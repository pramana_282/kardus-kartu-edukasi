<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRuteKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rute_kelas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_kelas',191)->unique();
            $table->integer('rute')->unsigned();
            $table->foreign('kode_kelas')->references('kode')->on('kelas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rute_kelas');
    }
}
