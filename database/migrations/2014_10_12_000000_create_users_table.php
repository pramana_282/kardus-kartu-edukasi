<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',100)->unique();
            $table->string('password',191);
            $table->string('nama',100);
            $table->enum('jk',['1','2']);
            $table->date('tanggal_lahir');
            $table->string('email',191)->unique();
            $table->string('instansi',191);
            $table->string('jabatan',100);
            $table->text('foto');
            $table->text('alamat');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
